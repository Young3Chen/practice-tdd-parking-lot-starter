package com.parkinglot;

import com.parkinglot.Exception.NoPositionException;
import com.parkinglot.Exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class ParkingManagerTest {

    @Test
    void should_add_a_parkingBoy_into_Parking_manager_list_when_addParkingBoy_given_all_kind_Parking_boys_and_parkingLotManger() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLot);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();

        //when
        parkingLotManager.addParkingBoy(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);
        parkingLotManager.addParkingBoy(standardParkingBoy);

        //then
        List<ParkingBoy> parkingBoys = parkingLotManager.getParkingBoys();
        assertEquals(parkingBoys.get(0), smartParkingBoy);
        assertEquals(parkingBoys.get(1), superSmartParkingBoy);
        assertEquals(parkingBoys.get(2), standardParkingBoy);
    }


    @Test
    void should_return_a_ticket_when_park_car_given_a_car_and_parkingLot_and_a_parkingLotManger() {
        //given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLotList = asList(parkingLot1, parkingLot2);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLotList);

        //when
        Ticket ticket = parkingLotManager.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_fetch_car_given_a_ticket_and_parked_parkingLot_and_parkingLotManger() {
        //given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLots);
        Ticket ticket = parkingLotManager.park(car);

        //when
        Car fetchedCar = parkingLotManager.fetch(ticket);

        //then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_right_car_when_fetch_car_twice_given_a_parkingBoy_with_parked_two_cars_and_parkingLotManger() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        StandardParkingBoy parkingBoy = new StandardParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);

        //when
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);

        //then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);

    }

    @Test
    void should_return_error_message_when_fetch_car_given_wrong_parking_ticket_and_parking_lot_and_parkingLotManger() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLots);
        Car car = new Car();
        parkingLotManager.park(car);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> parkingLotManager.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_error_message_when_fetch_car_given_used_parking_ticket_and_parking_lot_and_parkingLotManger() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLots);
        Car car = new Car();
        Ticket ticket = parkingLotManager.park(car);
        parkingLotManager.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> parkingLotManager.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }


    @Test
    void should_return_nothing_when_park_car_given_parking_lot_without_position_and_parkingLotManger() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLots);
        Car car = new Car();

        //when then
        NoPositionException noPositionException =
                Assertions.assertThrows(NoPositionException.class,
                        () -> parkingLotManager.park(car));
        assertEquals("No available position.", noPositionException.getMessage());
    }

    @Test
    void should_return_ticket_when_park_car_by_parkingBoy_given_parkingLotManager_managed_parkingBoys() {
        //given
        ParkingLotManager parkingLotManager = new ParkingLotManager(new SmartParkingBoy(new ParkingLot(2)));
        Car car = new Car();
        //when
        Ticket ticket = parkingLotManager.parkCarByParkingBoy(car);

        //then
        Assertions.assertNotNull(ticket);
    }
    @Test
    void should_return_car_when_park_car_by_parkingBoy_given_ticket_and_parkingLotManager_managed_parkingBoys() {
        //given
        ParkingLotManager parkingLotManager = new ParkingLotManager(new SmartParkingBoy(new ParkingLot(2)));
        Car car = new Car();
        Ticket ticket = parkingLotManager.parkCarByParkingBoy(car);

        //when
        Car carFetch = parkingLotManager.fetchCarByParkingBoy(ticket);

        //then
        assertEquals(car, carFetch);
    }

    @Test
    void should_return_error_message_when_park_car_by_parkingBoy_given_parkingLotManager_managed_parkingBoys() {
        //given
        ParkingLotManager parkingLotManager = new ParkingLotManager(new SmartParkingBoy(new ParkingLot(0)));
        Car car = new Car();
        //when then
        NoPositionException noPositionException =
                Assertions.assertThrows(NoPositionException.class,
                        () -> parkingLotManager.parkCarByParkingBoy(car));
        assertEquals("No available position.", noPositionException.getMessage());
    }

    @Test
    void should_return_error_message_when_fetch_car_by_parkingBoy_given_parkingLotManager_managed_parkingBoys() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLots);
        Car car = new Car();
        Ticket ticket = parkingLotManager.park(car);
        parkingLotManager.fetch(ticket);
        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> parkingLotManager.fetchCarByParkingBoy(ticket));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

}
