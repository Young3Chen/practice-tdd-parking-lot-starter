package com.parkinglot;

import com.parkinglot.Exception.NoPositionException;
import com.parkinglot.Exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SmartParkingBoyTest {
    @Test
    void should_return_a_ticket_in_a_large_position_parkingLot_when_park_car_given_a_car_and_parkingLot_and_a_smart_parkingBoy() {
        //given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(11);
        List<ParkingLot> parkingLotList = asList(parkingLot1, parkingLot2);
        SmartParkingBoy parkingBoy = new SmartParkingBoy(parkingLotList);

        //when
        Ticket ticket = parkingBoy.park(car);

        //then
        Assertions.assertFalse(parkingLot1.isCarInParkingLot(ticket));
        Assertions.assertTrue(parkingLot2.isCarInParkingLot(ticket));

    }

    @Test
    void should_return_a_car_when_fetch_car_given_a_ticket_and_parked_parkingLot_and_smart_parkingBoy() {
        //given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Ticket ticket = smartParkingBoy.park(car);

        //when
        Car fetchedCar = smartParkingBoy.fetch(ticket);

        //then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_right_car_when_fetch_car_twice_given_a_parkingBoy_with_parked_two_cars_and_smart_parkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = smartParkingBoy.park(car1);
        Ticket ticket2 = smartParkingBoy.park(car2);

        //when
        Car fetchedCar1 = smartParkingBoy.fetch(ticket1);
        Car fetchedCar2 = smartParkingBoy.fetch(ticket2);

        //then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);

    }

    @Test
    void should_return_error_message_when_fetch_car_given_wrong_parking_ticket_and_parking_lot_and_smart_parkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();
        smartParkingBoy.park(car);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> smartParkingBoy.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_error_message_when_fetch_car_given_used_parking_ticket_and_parking_lot_and_smart_parkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();
        Ticket ticket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> smartParkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }


    @Test
    void should_return_nothing_when_park_car_given_parking_lot_without_position_and_smart_parkingBoy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = asList(parkingLot1, parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();
        smartParkingBoy.park(new Car());
        smartParkingBoy.park(new Car());

        //when then
        NoPositionException noPositionException =
                Assertions.assertThrows(NoPositionException.class,
                        () -> smartParkingBoy.park(car));
        assertEquals("No available position.", noPositionException.getMessage());
    }
}
