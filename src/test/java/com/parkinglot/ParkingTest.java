package com.parkinglot;

import com.parkinglot.Exception.NoPositionException;
import com.parkinglot.Exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParkingTest {
    @Test
    void should_return_a_ticket_when_park_car_given_a_car_and_parkingLot_and_a_parkingBoy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        StandardParkingBoy parkingBoy = new StandardParkingBoy(parkingLot);

        //when
        Ticket ticket = parkingBoy.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_fetch_car_given_a_ticket_and_parked_parkingLot() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);

        //when
        Car fetchedCar = parkingLot.fetch(ticket);

        //then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_right_car_when_fetch_car_twice_given_a_parkingLot_with_parked_two_cars() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        //when
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        //then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);

    }

    @Test
    void should_return_error_message_when_fetch_car_given_wrong_parking_ticket_and_parking_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        parkingLot.park(car);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> parkingLot.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_error_message_when_fetch_car_given_used_parking_ticket_and_parking_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class,
                        () -> parkingLot.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }


    @Test
    void should_return_nothing_when_park_car_given_parking_lot_without_position() {
        //given
        ParkingLot parkingLot = new ParkingLot(0);
        Car car = new Car();

        //when then
        NoPositionException noPositionException =
                Assertions.assertThrows(NoPositionException.class,
                        () -> parkingLot.park(car));
        assertEquals("No available position.", noPositionException.getMessage());
    }
}
