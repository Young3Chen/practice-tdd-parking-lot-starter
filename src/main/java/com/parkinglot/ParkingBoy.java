package com.parkinglot;

import com.parkinglot.Exception.UnrecognizedTicketException;
import com.parkinglot.strategy.ParkStrategy;

import java.util.*;

public class ParkingBoy {

    final List<ParkingLot> parkingLots;

    private final ParkStrategy parkStrategy;

    protected final Map<Ticket, ParkingLot> carParkingLotMap = new HashMap<>();

    ParkingBoy(ParkingLot parkingLot, ParkStrategy parkStrategy) {
        parkingLots = new ArrayList<>();
        this.parkingLots.add(parkingLot);
        this.parkStrategy = parkStrategy;
    }

    ParkingBoy(ParkStrategy parkStrategy) {
        parkingLots = new ArrayList<>();
        this.parkStrategy = parkStrategy;
    }

    public ParkingBoy(List<ParkingLot> parkingLots, ParkStrategy parkStrategy) {
        this.parkingLots = parkingLots;
        this.parkStrategy = parkStrategy;
    }

    public Ticket park(Car car) {
        return parkStrategy.park(car,parkingLots, carParkingLotMap);
    }

    public Car fetch(Ticket ticket) {
        if (carParkingLotMap.containsKey(ticket)) {
            Car fetchCar = carParkingLotMap.get(ticket).fetch(ticket);
            carParkingLotMap.remove(ticket);
            return fetchCar;
        }
        throw new UnrecognizedTicketException();
    }

    boolean isParkingLotsLeftPosition() {
        long count = parkingLots.stream()
                .filter(ParkingLot::isLeftPosition)
                .count();
        return count > 0;
    }
}
