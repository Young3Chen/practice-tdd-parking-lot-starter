package com.parkinglot.strategy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.Ticket;

import java.util.List;
import java.util.Map;

public interface ParkStrategy {
    Ticket park(Car car, List<ParkingLot> parkingLots, Map<Ticket,ParkingLot> carParkingLotMap);

}
