package com.parkinglot.strategy;

import com.parkinglot.Car;
import com.parkinglot.Exception.NoPositionException;
import com.parkinglot.ParkingLot;
import com.parkinglot.Ticket;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class SmartParkStrategy implements ParkStrategy {
    @Override
    public Ticket park(Car car, List<ParkingLot> parkingLots, Map<Ticket, ParkingLot> carParkingLotMap) {
        ParkingLot parkingLot = parkingLots.stream()
                .filter(ParkingLot::isLeftPosition)
                .max(Comparator.comparing(ParkingLot::leftPosition))
                .orElseThrow(NoPositionException::new);
        Ticket ticket = parkingLot.park(car);
        carParkingLotMap.put(ticket, parkingLot);
        return ticket;
    }
}
