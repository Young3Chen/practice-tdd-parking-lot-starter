package com.parkinglot.strategy;

import com.parkinglot.Car;
import com.parkinglot.Exception.NoPositionException;
import com.parkinglot.ParkingLot;
import com.parkinglot.Ticket;

import java.util.List;
import java.util.Map;

public class StandardParkStrategy implements ParkStrategy {

    @Override
    public Ticket park(Car car, List<ParkingLot> parkingLots, Map<Ticket, ParkingLot> carParkingLotMap) {
        Ticket ticket = parkingLots.stream()
                .filter(ParkingLot::isLeftPosition)
                .findFirst()
                .map(parkingLot -> {
                    Ticket ticketReturn = parkingLot.park(car);
                    carParkingLotMap.put(ticketReturn, parkingLot);
                    return ticketReturn;
                })
                .orElseThrow(NoPositionException::new);
        return ticket;
    }
}
