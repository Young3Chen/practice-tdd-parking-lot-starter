package com.parkinglot;

import com.parkinglot.Exception.NoPositionException;
import com.parkinglot.Exception.UnrecognizedTicketException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    
    private final int capital ;
    private final Map<Ticket,Car> parkedCars;

    ParkingLot() {
        parkedCars = new HashMap<>();
        capital = 10;
    }
    ParkingLot(int capital){
        parkedCars = new HashMap<>();
        this.capital = capital;
    }

    public Ticket park(Car car) {
        if(!isLeftPosition()) {
            throw  new NoPositionException();
        }
        Ticket ticket = new Ticket();
        parkedCars.put(ticket, car);
        return ticket;
    }

    public boolean isLeftPosition() {
        return capital > parkedCars.size();
    }

    public int leftPosition(){
        return capital - parkedCars.size();
    }

    public float leftPositionRate(){
        return (float) leftPosition() / parkedCars.size();
    }

    public Car fetch(Ticket ticket) {
        if (!parkedCars.containsKey(ticket)) {
            throw new UnrecognizedTicketException();
        }
        return parkedCars.remove(ticket);
    }

    public boolean isCarInParkingLot(Ticket ticket) {
        return parkedCars.containsKey(ticket);
    }
}
