package com.parkinglot;

import com.parkinglot.strategy.SuperSmartParkStrategy;

import java.util.List;

public class SuperSmartParkingBoy extends ParkingBoy {
    SuperSmartParkingBoy() {
        super(new SuperSmartParkStrategy());
    }
    public SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots, new SuperSmartParkStrategy());
    }

}
