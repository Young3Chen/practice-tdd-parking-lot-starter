package com.parkinglot.Exception;

public class NoPositionException extends RuntimeException{
    public NoPositionException() {
        super("No available position.");
    }
}
