package com.parkinglot.Exception;

public class UnrecognizedTicketException extends RuntimeException{
    public UnrecognizedTicketException() {
        super("Unrecognized parking ticket.");
    }
}
