package com.parkinglot;

import com.parkinglot.strategy.SmartParkStrategy;
import com.parkinglot.strategy.StandardParkStrategy;

import java.util.List;

public class StandardParkingBoy extends ParkingBoy{

    StandardParkingBoy(){super(new StandardParkStrategy());}
    StandardParkingBoy(ParkingLot parkingLot) {
        super(parkingLot,new StandardParkStrategy());
    }

    public StandardParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots,new StandardParkStrategy());
    }

}
