package com.parkinglot;

import com.parkinglot.strategy.SmartParkStrategy;

import java.util.List;

public class SmartParkingBoy extends ParkingBoy {

    public SmartParkingBoy(List<ParkingLot> parkingLotList) {
        super(parkingLotList, new SmartParkStrategy());
    }

    public SmartParkingBoy() {
        super(new SmartParkStrategy());
    }

    public SmartParkingBoy(ParkingLot parkingLot) {
        super(parkingLot, new SmartParkStrategy());
    }
}
