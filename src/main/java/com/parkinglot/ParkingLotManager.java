package com.parkinglot;

import com.parkinglot.Exception.NoPositionException;
import com.parkinglot.Exception.UnrecognizedTicketException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParkingLotManager extends StandardParkingBoy {

    private final List<ParkingBoy> parkingBoys = new ArrayList<>();
    private final Map<Ticket, ParkingBoy> ticketParkingBoyMap = new HashMap<>();

    ParkingLotManager(ParkingLot parkingLot) {
        super(parkingLot);
    }

    public ParkingLotManager(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }


    public ParkingLotManager(ParkingBoy parkingBoy) {
        parkingBoys.add(parkingBoy);
    }

    public void addParkingBoy(ParkingBoy parkingBoy) {
        if (parkingBoy == null) return;
        parkingBoys.add(parkingBoy);
    }

    public List<ParkingBoy> getParkingBoys() {
        return this.parkingBoys;
    }

    public Ticket parkCarByParkingBoy(Car car) {
        Ticket ticket = parkingBoys.stream()
                .filter(ParkingBoy::isParkingLotsLeftPosition)
                .findFirst()
                .map(parkingBoy -> {
                    Ticket ticketBack = parkingBoy.park(car);
                    ticketParkingBoyMap.put(ticketBack, parkingBoy);
                    return ticketBack;
                })
                .orElseThrow(NoPositionException::new);
        return ticket;
    }

    public Car fetchCarByParkingBoy(Ticket ticket) {
        if (!ticketParkingBoyMap.containsKey(ticket)) {
            throw new UnrecognizedTicketException();
        }
        return ticketParkingBoyMap.get(ticket).fetch(ticket);
    }
}
